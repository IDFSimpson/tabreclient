'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ListView,
  TouchableHighlight,
  View,
} from 'react-native';

var AjaxRequests = require('./AjaxRequests');

class BookmarksIndex extends Component {
  constructor() {
    super();
  }

  render() {
    if (!this.props.loaded) {
      return this.renderLoadingView();
    }

    return (
      <ListView
        dataSource = {this.props.bookmarks}
        renderRow = {this.renderBookmark.bind(this) }
        style = {styles.listView}
      />
    );
  }

  renderLoadingView() {
    return (
      <View style={styles.container}>
        <Text>Loading ... </Text>
      </View>
    );
  }

  renderBookmark(bookmark) {
    return (
      <TouchableHighlight onPress={() => {
        this.props.navigator.push({ name: 'bookmarkScreen', bookmark: bookmark.id, bookmarkName: bookmark.name, navigator: this.props.navigator });
      }}>
        <View style={styles.container}>
          <Text style={{fontWeight: 'bold'}}>{bookmark.name}</Text>
          <Text>{bookmark.url}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    padding: 20,
    margin: 5,
  },
   listView: {
     margin: 10,
     paddingTop: 20,
     backgroundColor: '#F5FCFF',
 },
});

module.exports = BookmarksIndex;
